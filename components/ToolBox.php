<?php
/**
 * TechMarket Theme
 * Created by alvaro.
 * User: alvaro
 * Date: 27/02/18
 * Time: 06:14 AM
 */

namespace PlanetaDelEste\TechMarket\Components;


use Cms\Classes\ComponentBase;
use Cms\Classes\ComponentManager;
use Cms\Classes\Page;
use Lovata\Buddies\Facades\AuthHelper;
use Lovata\OrdersShopaholic\Models\Order;
use Lovata\Shopaholic\Models\Settings;
use System\Classes\PluginManager;

class ToolBox extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'planetadeleste.techmarket::lang.components.toolbox.name',
            'description' => 'planetadeleste.techmarket::lang.components.toolbox.description'
        ];
    }

    public function onRun()
    {
        $this->page['shopCurrency'] = Settings::getValue('currency');
        $pageId = $this->page->page->getId();
        if (in_array($pageId, ['shop-category-list', 'shop-brands', 'shop-search'])) {
            $this->onControlBar();
        }

        if ($pageId == 'account' && AuthHelper::check()) {
            /** @var \Lovata\Buddies\Components\UserPage $userPageComp */
            $userPageComp = $this->page->components['UserPage'];
            $userPageComp->setProperty('slug', AuthHelper::getUser()->id);

            $this->onLoadPartial();
        }
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function hasComponent($name): bool
    {
        $manager = ComponentManager::instance();
        return $manager->hasComponent($name);
    }

    /**
     * @param $namespace
     *
     * @return bool
     */
    public function hasPlugin($namespace): bool
    {
        $manager = PluginManager::instance();
        return $manager->hasPlugin($namespace);
    }

    public function loadComponent($className, $alias = null, $options = [])
    {
        if ($this->hasComponent($className)) {
            if (!$alias) {
                $parts = explode('\\', $className);
                $alias = array_pop($parts);
            }
            $this->addComponent($className, $alias, $options);
        }
    }

    public function onControlBar()
    {
        /** @var \Lovata\Shopaholic\Components\CategoryPage $categoryPageComp */
        /** @var \Lovata\Shopaholic\Components\ProductPage $productListComp */
        /** @var \Lovata\Toolbox\Components\Pagination $paginationComp */
        /** @var \Lovata\Shopaholic\Components\BrandPage $brandComp */

        $productListComp = $this->page->components['ProductList'];
        $paginationComp = $this->page->components['Pagination'];

//        $view = post('view', 'grid');
        $productList = $productListComp->make()->sort($productListComp->getSorting())->active();
        $pageId = $this->page->page->getId();
        switch ($pageId) {
            case 'shop-category-list':
                $categoryPageComp = $this->page->components['CategoryPage'];
                /** @var \Lovata\Shopaholic\Classes\Item\CategoryItem $category */
                $category = $categoryPageComp->get();
                $productList = $productList->category($category->id);
                if ($category->children->isNotEmpty()) {
                    foreach ($category->children as $obChildCategory) {
                        if ($obChildCategory->product_count) {
                            $childProductList = $productListComp->make()->sort($productListComp->getSorting())->active(
                            )->category($obChildCategory->id);
                            $productList = $productList->merge($childProductList->getIDList());
                        }
                    }
                }
                break;

            case 'shop-brands':
                $brandComp = $this->page->components['BrandPage'];
                $brand = $brandComp->get();
                $productList = $productList->brand($brand->id);
                break;

            case 'shop-search':
                if ($this->hasPlugin('Lovata\SearchShopaholic')) {
                    list($query, $categoryId) = array_pad(explode(':', $this->param('q')), 2, null);
                    $this->page['sSearch'] = $query;
                    $productList = $productList->search($query);
                    if ($categoryId) {
                        $this->page['productCat'] = $categoryId;
                        $productList = $productList->category($categoryId);
                    }
                }
                break;

            default:
                return null;
                break;
        }


        $iPage = $this->param('page', 1);
        $paginationList = $paginationComp->get($iPage, $productList->count());
        $arProductList = $productList->page($iPage, $paginationComp->getCountPerPage());

        $this->page['obProductList'] = $productList;
        $this->page['iPage'] = $iPage;
        $this->page['arPaginationList'] = $paginationList;
        $this->page['arProductList'] = $arProductList;

//        return ['#'.$view => $this->renderPartial('components/product-'.$view, $partialData)];
    }

    public function onProductSearch()
    {
        $query = post('s');
        if ($category = post('product_cat')) {
            $query .= ':'.$category;
        }

        $url = Page::url('shop-search', ['q' => $query]);

        return redirect($url);

//        if(!$this->hasPlugin('Lovata\SearchShopaholic')) {
//            \Flash::error();
//        }
    }

    public function onLoadPartial()
    {
        /** @var \Lovata\Buddies\Classes\Item\UserItem $obUser */
        $this->page['obUser'] = $obUser = $this->page->layout->components['UserData']->get();
//        if (post('section') == 'orders') {
//            $this->page['orders'] = $this->getUserOrders($obUser->id);
//        }

        if ($this->param('section') == 'orders') {
            $this->page['orders'] = $this->getUserOrders(AuthHelper::getUser()->id);
            if ($this->param('param1') == 'order' && $this->param('param2')) {
                $this->page['order'] = $this->getUserOrders(AuthHelper::getUser()->id, $this->param('param2'));
            }
        }
    }

    /**
     * @param integer     $user_id
     * @param null|string $key
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Lovata\OrdersShopaholic\Models\Order|\Lovata\OrdersShopaholic\Models\Order[]
     */
    public function getUserOrders($user_id, $key = null)
    {
        return ($key) ? Order::getBySecretKey($key)->first() : Order::where('user_id', $user_id)
            ->orderBy('created_at', 'desc')
            ->get();
    }

}